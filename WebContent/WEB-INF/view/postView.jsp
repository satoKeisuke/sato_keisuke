<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="jp.alhinc.sato_keisuke.bulletin_board_system.model.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>新規投稿</title>
</head>
<body>
新規投稿
<hr>
<%
error error = (error)session.getAttribute("error");
String subject = "";
String text = "";
String category = "";

String subjectErr = "";
String textErr = "";
String categoryErr = "";

if (error != null) {
	subjectErr = error.getFormatErrMessage(error.getSubject());
	textErr = error.getFormatErrMessage(error.getText());
	categoryErr = error.getFormatErrMessage(error.getCategory());
}

postBean post = new postBean();
post = (postBean)session.getAttribute("post");

if (post != null) {
	subject = post.getSubject();
	text = post.getText();
	category = post.getCategory();
}
%>
<FORM method="POST" action="postController">
  件名  <font color="red"><%= subjectErr %></font><br>
  <INPUT type="text" name="subject" style="width:300px;" value=<%= subject %>><br>
  カテゴリー  <font color="red"><%= categoryErr %></font><br>
  <INPUT type="text" name="category" value=<%= category %>><br>
  本文  <font color="red"><%= textErr %></font><br>
  <textarea name="text" style="width:800px;height:200px;"><%= text %></textarea><br>
  <br>
  <INPUT type="submit" name="post" value="投稿">
</FORM>
<br>
<hr>
<a href="homeController">ホームへ</a>
</body>
</html>