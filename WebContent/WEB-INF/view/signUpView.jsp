<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ユーザー新規登録画面</title>
</head>
<body>
ユーザー新規登録画面
<hr>
    <FORM method="POST" action="signUpController">
      ログインID<br>
      <INPUT type="text" name="loginId"><br>
      名称<br>
      <INPUT type="text" name="name"><br>
      支店<br>
      <INPUT type="text" name="store"><br>
      部署・役職<br>
      <INPUT type="text" name="position"><br>
      パスワード<br>
      <INPUT type="password" name="password"><br>
      パスワード（確認用）<br>
      <INPUT type="password" name="confirmation"><br>
      <br>
      <INPUT type="submit" name="post" value="登録">
    </FORM>
<br>
<hr>
<a href="homeController">ホームへ</a>
</body>
</html>