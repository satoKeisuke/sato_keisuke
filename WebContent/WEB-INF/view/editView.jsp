<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="jp.alhinc.sato_keisuke.bulletin_board_system.model.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ユーザー編集画面</title>
</head>
<body>
ユーザー編集画面
<hr>
■変更前情報<br>
<%
userBean oldUser = new userBean();
userFieldBean newUser = new userFieldBean();
oldUser = (userBean)session.getAttribute("oldUser");
newUser = (userFieldBean)session.getAttribute("newUser");
storePositionBean storePosition = (storePositionBean)session.getAttribute("storePosition");
%>
ログインID：<%= oldUser.getLoginId() %><br>
名称：<%= oldUser.getName() %><br>
支店：<%= oldUser.getStore() %> = <%= storePosition.getStoreMap().get(oldUser.getStore()) %><br>
部署・役職：<%= oldUser.getPosition() %> = <%= storePosition.getPositionMap().get(oldUser.getPosition()) %><br>
パスワード：<%= oldUser.getPassword() %><br>

<br>
■変更後情報<br>
<%
error error = (error)session.getAttribute("error");
String newLoginId = "";
String newName = "";
String newStore = "";
String newPosition = "";

String newLoginIdErr = "";
String newNameErr = "";
String newStoreErr = "";
String newPositionErr = "";
String newPasswordErr = "";
String newConfirmationErr = "";
if (error != null) {
	newLoginIdErr = error.getFormatErrMessage(error.getLoginId());;
	newNameErr = error.getFormatErrMessage(error.getName());;
	newStoreErr = error.getFormatErrMessage(error.getStore());
	newPositionErr = error.getFormatErrMessage(error.getPosition());
	newPasswordErr = error.getFormatErrMessage(error.getPassword());
	newConfirmationErr = error.getPassConfErrMessage(error.getConfirmation());
}

if (newUser.getLoginId() != null) {
	newLoginId = newUser.getLoginId();
}
if (newUser.getName() != null) {
	newName = newUser.getName();
}
if (newUser.getStore() != null) {
	newStore = newUser.getStore();
}
if (newUser.getPosition() != null) {
	newPosition = newUser.getPosition();
}
%>
<FORM method="POST" action="editController">
      ログインID<br>
      <INPUT type="text" name="newLoginId" value= <%= newLoginId %>><font color="red"><%= newLoginIdErr %></font><br>
      名称<br>
      <INPUT type="text" name="newName" value= <%= newName %>><font color="red"><%= newNameErr %></font><br>
      支店<br>
      <INPUT type="text" name="newStore" value= <%= newStore %>><font color="red"><%= newStoreErr %></font><br>
      部署・役職<br>
      <INPUT type="text" name="newPosition" value= <%= newPosition %>><font color="red"><%= newPositionErr %></font><br>
      パスワード<br>
      <INPUT type="password" name="newPassword"><font color="red"><%= newPasswordErr %></font><br>
      パスワード（確認用）<br>
      <INPUT type="password" name="newConfirmation"><font color="red"><%= newConfirmationErr %></font><br>
      <br>
      <INPUT type="submit" name="post" value="登録">
</FORM>
<hr>
<a href="homeController">ホームへ</a>
</body>
</html>
