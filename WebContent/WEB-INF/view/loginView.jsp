<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="jp.alhinc.sato_keisuke.bulletin_board_system.model.userBean"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>login</title>
</head>
<body>
	<FORM method="POST" action="loginController">
      ログインID : <INPUT type="text" name="loginId"><br><br>
      パスワード : <INPUT type="password" name="password"><br><br>
      <%
      if(request.getAttribute("auth") == "0") {
      %>
      <font color="red">ログインID、もしくはパスワードが間違っています。</font><br>
      <%
      }
      %>
      <INPUT type="submit" name="pagename" value="ログイン">
      <INPUT type="reset" value="クリア">
    </FORM>
</body>
</html>