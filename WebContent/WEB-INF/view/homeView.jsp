<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="jp.alhinc.sato_keisuke.bulletin_board_system.model.userBean"%>
<%@ page import="jp.alhinc.sato_keisuke.bulletin_board_system.model.homeBean"%>
<%@ page import="java.util.ArrayList"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>home</title>
</head>
<body>
ホーム
<hr>
<%
userBean user = new userBean();
user = (userBean)session.getAttribute("user");
String name = user.getName();
%>
<%= name %>さん<br>
<br>
メニュー：
[<a href="postController">新規投稿</a>]
[<a href="managementController">ユーザー管理</a>]
[<a href="logout">ログアウト</a>]
<br>
<br>
<FORM method="POST" action="homeController">
	カテゴリー：<INPUT type="text" name="category"><br>
	投稿日時：
	<select name="fromYear" id="idFromYear"></select>
	<select name="fromMonth" id="idFromMonth"></select>
	<select name="fromDay" id="idFromDay"></select>
	～
	<select name="toYear" id="idToYear"></select>
	<select name="toMonth" id="idToMonth"></select>
	<select name="toDay" id="idToDay"></select>
	<script>
	(function() {
	  'use strict';
	  var optionLoop, this_day, this_month, this_year, today;
	  today = new Date();
	  this_year = today.getFullYear();
	  this_month = today.getMonth() + 1;
	  this_day = today.getDate();

	  optionLoop = function(start, end, id, this_day) {
	    var i, opt;

	    opt = null;
	    for (i = start; i <= end ; i++) {
	      if (i === this_day) {
	        opt += "<option value='" + i + "' selected>" + i + "</option>";
	      } else {
	        opt += "<option value='" + i + "'>" + i + "</option>";
	      }
	    }
	    return document.getElementById(id).innerHTML = opt;
	  };
	  optionLoop(1950, this_year, 'idFromYear', 1950);
	  optionLoop(1, 12, 'idFromMonth', 1);
	  optionLoop(1, 31, 'idFromDay', 1);
	  optionLoop(1950, this_year, 'idToYear', this_year);
	  optionLoop(1, 12, 'idToMonth', this_month);
	  optionLoop(1, 31, 'idToDay', this_day);
	})();
	</script>
	<INPUT type="submit" name="pagename" value="検索">
</FORM>
<hr>
<%
ArrayList<String[][]> postList = new ArrayList<>();
postList = (ArrayList<String[][]>)request.getAttribute("postList");

ArrayList<ArrayList<String[]>[]> commentList = new ArrayList<>();
commentList = (ArrayList<ArrayList<String[]>[]>)request.getAttribute("commentList");

String subject;
String text;
String category;
String regiDate;
String regiUser;
String postId;
int pageNum = 1;
if (session.getAttribute("home") != null) {
	pageNum = ((homeBean)session.getAttribute("home")).getCurrentPage();
}

int allPages = postList.size();

if (postList.size() > 0) {
	for (int i = 0; i < 5; i++) {
		subject = postList.get(pageNum - 1)[i][0];
		text = postList.get(pageNum - 1)[i][1];
		category = postList.get(pageNum - 1)[i][2];
		regiDate = postList.get(pageNum - 1)[i][3];
		regiUser = postList.get(pageNum - 1)[i][4];
		postId = postList.get(pageNum - 1)[i][5];
		if (postList.get(pageNum - 1)[i][0] != null) {
%>
タイトル：<%= subject %><br>
内容：<br>　<%= text.replaceAll("\r\n","<br>　") %><br>
カテゴリー：<%= category %><br>
登録日時：<%= regiDate %><br>
登録者：<%= regiUser %><br>

<%
			if (regiUser.equals(((userBean)session.getAttribute("user")).getLoginId())) {
%>
<FORM method="POST" action="homeController">
	<INPUT type="hidden" name="id" value=<%= postId %>>
	<INPUT type="hidden" name="target" value="post">
	<INPUT type="submit" name="pagename" value="削除">
</FORM>
<%
			}
%>
<%
			ArrayList<String[]>[] commentArray = commentList.get(pageNum - 1);
				if (commentArray[i].size() > 0) {
%>
--- コメント ------------------------------------------------------------------------<br>
<%
				String commentText;
				String commentRegiDate;
				String commentRegiUser;
					for (int j = 0; j < commentArray[i].size(); j++) {
						commentText = commentArray[i].get(j)[0];
						commentRegiDate = commentArray[i].get(j)[1];
						commentRegiUser = commentArray[i].get(j)[2];
%>
　　　内容：<br>　　　　　<%= commentText.replaceAll("\r\n","<br>　　　　　") %><br>
　　　登録日時：<%= commentRegiDate %><br>
　　　登録者：<%= commentRegiUser %>
<%
if (commentRegiUser.equals(((userBean)session.getAttribute("user")).getLoginId())) {
%>
<FORM method="POST" action="homeController">
	<INPUT type="hidden" name="id" value=<%= commentArray[i].get(j)[3] %>>
	<INPUT type="hidden" name="target" value="comment">
	<INPUT type="submit" name="pagename" value="削除">
</FORM>
<%
} else {
%>
<br>
<%
}
%>
--------------------------------------------------------------------------------------<br>
<%
					}
				}
%>
<FORM method="POST" action="homeController">
	<INPUT type="hidden" name="postId" value=<%= postId %>>
	<br>
	<textarea name="comment" style="width:600px;height:80px;"></textarea><br>
	<INPUT type="hidden" name="jumpPageNum" value=<%= pageNum %>>
	<INPUT type="submit" name="pagename" value="コメント">
</FORM>
<hr>
<%
		}
	}
}
%>
<<
<%
	int[] prePageArray = new int[3];
	int[] bePageArray = new int[3];
	for (int i = 2; i > -1; i--) {
		prePageArray[i] = pageNum - i - 1;
	}
	for (int i = 0; i < 3; i++) {
		bePageArray[i] = pageNum + i + 1;
	}
	ArrayList<Integer> pageList = new ArrayList<>();
	for (int i = prePageArray.length - 1; i >= 0 ; i--) {
		if (prePageArray[i] > 0) {
			pageList.add(prePageArray[i]);
		}
	}
	pageList.add(pageNum);
	for (int i = 0; i < bePageArray.length; i++) {
		if (bePageArray[i] <= allPages) {
			pageList.add(bePageArray[i]);
		}
	}
	for (int i = 0; i < pageList.size(); i++) {
		if (pageList.get(i) == pageNum) {
%>
<%= pageList.get(i) %>
<%
		} else {
%>
<a href="homeController?jumpPageNum=<%= pageList.get(i) %>"><%= pageList.get(i) %></a>
<%
		}
	}
%>
>>
</body>
</html>