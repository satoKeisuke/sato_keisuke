<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="jp.alhinc.sato_keisuke.bulletin_board_system.model.storePositionBean"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ユーザー管理画面</title>
</head>
<body>
ユーザー管理画面
<hr>
<a href="signUpController">ユーザー新規登録</a>
<br>
<br>
<%
ArrayList<String[]> userList = new ArrayList<>();
userList = (ArrayList<String[]>)request.getAttribute("userList");
String loginId;
String password;
String name;
String store;
String position;
String user;
storePositionBean storePosition = (storePositionBean)session.getAttribute("storePosition");
System.out.println(storePosition);
for (int i = 0; i < userList.size(); i++) {
	loginId = userList.get(i)[0];
	password = userList.get(i)[1];
	name = userList.get(i)[2];
	store = userList.get(i)[3];
	position = userList.get(i)[4];
	user = userList.get(i)[5];
%>
ログインID：<%= loginId %><br>
パスワード：<%= password %><br>
ユーザー名：<%= name %><br>
支店：<%= store %> = <%= storePosition.getStoreMap().get(Integer.parseInt(store)) %><br>
部署・役職：<%= position %> = <%= storePosition.getPositionMap().get(Integer.parseInt(position)) %><br>
有効(1) / 無効(0)：<%= user %><br>

<div style="display:inline-flex">
<FORM method="POST" action="editController">
<INPUT type="hidden" name="oldLoginId" value=<%= loginId %>>
<INPUT type="hidden" name="oldPassword" value=<%= password %>>
<INPUT type="hidden" name="oldName" value=<%= name %>>
<INPUT type="hidden" name="oldStore" value=<%= store %>>
<INPUT type="hidden" name="oldPosition" value=<%= position %>>
<INPUT type="hidden" name="oldUser" value=<%= user %>>
<INPUT type="hidden" name="control" value="management">
<INPUT type="submit" name="pagename" value="編集">
</FORM>
<FORM method="POST" action="managementController">
<INPUT type="hidden" name="loginId" value=<%= loginId %>>
<INPUT type="hidden" name="user" value=<%= user %>>
<%
if (user.equals("1")) {
%>
<INPUT type="submit" name="pagename" value="停止">
<%
} else {
%>
<INPUT type="submit" name="pagename" value="復活">
<%
}
%>
</FORM>
</div>
<br>
<br>
<%
}
%>
<hr>
<a href="homeController">ホームへ</a>
</body>
</html>