package jp.alhinc.sato_keisuke.bulletin_board_system.model;

import java.io.Serializable;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

public class storePositionBean implements Serializable {
  private HashMap<Integer, String> storeMap;
  private HashMap<Integer, String> positionMap;

  /** Creates a new instance of UserBean */
    public storePositionBean() {
    	storeMap = new HashMap<>();
    	sqls sqls = new sqls();
		String sql = sqls.getStoresSql();
		sqlExecute execute = new sqlExecute();
    	try {
	        ResultSet result = execute.executeQuery(sql);
			while(result.next()){
			    storeMap.put(result.getInt("code"), result.getString("name"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
        execute.closeConcection();

        positionMap = new HashMap<>();
        sql = sqls.getPositionsSql();
        execute = new sqlExecute();
        try {
	        ResultSet result = execute.executeQuery(sql);
			while(result.next()){
			    positionMap.put(result.getInt("code"), result.getString("name"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
        execute.closeConcection();
    }

public HashMap<Integer, String> getStoreMap() {
	return storeMap;
}

public void setStoreMap(HashMap<Integer, String> storeMap) {
	this.storeMap = storeMap;
}

public HashMap<Integer, String> getPositionMap() {
	return positionMap;
}

public void setPositionMap(HashMap<Integer, String> positionMap) {
	this.positionMap = positionMap;
}

}