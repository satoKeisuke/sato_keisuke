package jp.alhinc.sato_keisuke.bulletin_board_system.model;

import java.io.Serializable;

public class error implements Serializable {
  //ユーザー
  private Boolean loginId;
  private Boolean password;
  private Boolean name;
  private Boolean store;
  private Boolean position;
  private Boolean confirmation;
  private Boolean permission;

  //投稿
  private Boolean subject;
  private Boolean text;
  private Boolean category;

  public final String permissionErr = "アクセス権限がありません。";
  public final String formatErr = "項目定義に違反しています。";
  public final String passwordConfirmErr = "パスワードが一致しません。";
  public final String loginErr = "ログインID、もしくはパスワードが間違っています。";
  public final String updateErr = "更新に失敗しました。";
  public final String postErr = "投稿に失敗しました。";

  /** Creates a new instance of UserBean */
    public error() {
    	setLoginId(true);
    	setPassword(true);
    	setName(true);
    	setStore(true);
    	setPosition(true);
    	setConfirmation(true);
    	setPermission(true);
    	setSubject(true);
    	setText(true);
    	setCategory(true);
    }

    /**
     * 項目定義エラーのメッセージを返す
     * @param flg true:違反なし、false:違反あり
     * @return エラーメッセージ
     */
    public String getFormatErrMessage(boolean flg) {
    	String message = "";
    	if (!flg) {
    		message = formatErr;
    	}
    	return message;
    }

    /**
     * パスワード確認欄のエラーメッセージを返す
     * @param flg true:違反なし、false:違反あり
     * @return エラーメッセージ
     */
    public String getPassConfErrMessage(boolean flg) {
    	String message = "";
    	if (!flg) {
    		message = passwordConfirmErr;
    	}
    	return message;
    }

	public Boolean getLoginId() {
		return loginId;
	}

	public void setLoginId(Boolean loginId) {
		this.loginId = loginId;
	}

	public Boolean getPassword() {
		return password;
	}

	public void setPassword(Boolean password) {
		this.password = password;
	}

	public Boolean getName() {
		return name;
	}

	public void setName(Boolean name) {
		this.name = name;
	}

	public Boolean getStore() {
		return store;
	}

	public void setStore(Boolean store) {
		this.store = store;
	}

	public Boolean getPosition() {
		return position;
	}

	public void setPosition(Boolean position) {
		this.position = position;
	}

	public Boolean getConfirmation() {
		return confirmation;
	}

	public void setConfirmation(Boolean confirmation) {
		this.confirmation = confirmation;
	}

	public Boolean getPermission() {
		return permission;
	}

	public void setPermission(Boolean permission) {
		this.permission = permission;
	}

	public Boolean getSubject() {
		return subject;
	}

	public void setSubject(Boolean subject) {
		this.subject = subject;
	}

	public Boolean getText() {
		return text;
	}

	public void setText(Boolean text) {
		this.text = text;
	}

	public Boolean getCategory() {
		return category;
	}

	public void setCategory(Boolean category) {
		this.category = category;
	}
}