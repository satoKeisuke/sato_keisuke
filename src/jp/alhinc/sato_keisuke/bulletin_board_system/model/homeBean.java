package jp.alhinc.sato_keisuke.bulletin_board_system.model;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class homeBean implements Serializable {
  private int currentPage;
  private String fromTerm;
  private String toTerm;
  private String category;

  /** Creates a new instance of UserBean */
    public homeBean() {
    	setCurrentPage(1);
    	setFromTerm("1950-1-1 00:00:00");
    	Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        sdf.format(cal.getTime());
    	setToTerm(sdf.format(cal.getTime()));
    	setCategory("");
    }

public int getCurrentPage() {
	return currentPage;
}

public void setCurrentPage(int currentPage) {
	this.currentPage = currentPage;
}

public String getFromTerm() {
	return fromTerm;
}

public void setFromTerm(String fromTerm) {
	this.fromTerm = fromTerm;
}

public String getToTerm() {
	return toTerm;
}

public void setToTerm(String toTerm) {
	this.toTerm = toTerm;
}

public String getCategory() {
	return category;
}

public void setCategory(String category) {
	this.category = category;
}
}