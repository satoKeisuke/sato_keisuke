package jp.alhinc.sato_keisuke.bulletin_board_system.model;

import java.io.Serializable;

public class postBean implements Serializable {
  private String subject;
  private String text;
  private String category;

  /** Creates a new instance of UserBean */
    public postBean() {
    	setSubject("");
    	setText("");
    	setCategory("");
    }

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}
}