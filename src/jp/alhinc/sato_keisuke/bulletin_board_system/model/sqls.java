package jp.alhinc.sato_keisuke.bulletin_board_system.model;

import java.util.ArrayList;

public class sqls {
	public String getLoginSql(String loginId, String password) {
		String sql;
		sql = "select * from users where login_id = \"" + loginId + "\" and password = \"" + password + "\"";

		return sql;
	}

	public String getUsersSql() {
		String sql;
		sql = "select * from users";

		return sql;
	}

	public String getCommentsSql(String id) {
		String sql;
		sql = "select * from comments where post_id = \"" + id + "\" order by registered_date desc";

		return sql;
	}

	public String getPostsFilterSql(String category, String from, String to) {
		String sql;
		if (category.equals("")) {
			if (from.equals("")) {
				sql = "select * from posts order by registered_date desc";
			} else {
				sql = "select * from posts where registered_date BETWEEN \"" + from + "\" AND \"" + to + "\" order by registered_date desc";
			}
		} else {
			sql = "select * from posts where (category like \"%" + category + "%\") AND (registered_date BETWEEN \"" + from + "\" AND \"" + to + "\") order by registered_date desc";
		}

		return sql;
	}

	public String getSignUpSql(String loginId, String password, String name, int store, int position) {
		String sql;
		sql = "INSERT INTO users (login_id, password, name, store, departments_positions, user) VALUES (\"" + loginId + "\", \"" + password + "\", \"" + name + "\", " + store + ", " + position + ", 1)";

		return sql;
	}

	public String getPostSql(String subject, String text, String category, String registered_date, String registered_user) {
		String sql;
		sql = "INSERT INTO posts (subject, text, category, registered_date, registered_user) VALUES (\"" + subject + "\", \"" + text + "\", \"" + category + "\", \"" + registered_date + "\", \"" + registered_user + "\")";

		return sql;
	}

	public String getPostCommentSql(String text, String registered_date, String registered_user, String post_id) {
		String sql;
		sql = "INSERT INTO comments (text, registered_date, registered_user, post_id) VALUES (\"" + text + "\", \"" + registered_date + "\", \"" + registered_user + "\", \"" + post_id + "\")";

		return sql;
	}

	public String getCommentDeleteSql(String id) {
		String sql;
		sql = "delete from comments where id = \"" + id + "\"";

		return sql;
	}

	public String getPostDeleteSql(String id) {
		String sql;
		sql = "delete from posts where id = \"" + id + "\"";

		return sql;
	}

	public String getPostCommentDeleteSql(String postId) {
		String sql;
		sql = "delete from comments where post_id = \"" + postId + "\"";

		return sql;
	}

	public String getStoresSql() {
		String sql;
		sql = "select * from stores";

		return sql;
	}

	public String getPositionsSql() {
		String sql;
		sql = "select * from departments_positions";

		return sql;
	}

	public ArrayList<String> getEditSql(String oldLoginId, String newLoginId, String newPassword, String newName, String newStore, String newPosition) {
		ArrayList<String> sqlList = new ArrayList<>();
		if (!newPassword.equals("")) {
			sqlList.add("update users set password = \"" + newPassword + "\" where login_id = \"" + oldLoginId + "\"");
		}
		if (!newName.equals("")) {
			sqlList.add("update users set name = \"" + newName + "\" where login_id = \"" + oldLoginId + "\"");
		}
		if (!newStore.equals("")) {
			sqlList.add("update users set store = \"" + newStore + "\" where login_id = \"" + oldLoginId + "\"");
		}
		if (!newPosition.equals("")) {
			sqlList.add("update users set departments_positions = \"" + newPosition + "\" where login_id = \"" + oldLoginId + "\"");
		}
		if (!newLoginId.equals("")) {
			sqlList.add("update users set login_id = \"" + newLoginId + "\" where login_id = \"" + oldLoginId + "\"");
		}

		return sqlList;
	}

	public String getChangeSql(String loginId, String user) {
		String sql;
		if (user.equals("1")) {
			user = "0";
		} else {
			user = "1";
		}
		sql = "update users set user = \"" + user + "\" where login_id = \"" + loginId + "\"";

		return sql;
	}
}


