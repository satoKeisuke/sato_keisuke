package jp.alhinc.sato_keisuke.bulletin_board_system.model;

import java.io.Serializable;

public class userFieldBean implements Serializable {
  private String loginId;
  private String password;
  private String confirmation;
  private String name;
  private String store;
  private String position;

  /** Creates a new instance of UserBean */
    public userFieldBean() {
    	loginId = "";
    	password = "";
    	confirmation = "";
    	name = "";
    	store = "";
    	position = "";
    }

    public String getLoginId() {
    	return loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getStore() {
		return store;
	}

	public void setStore(String store) {
		this.store = store;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getConfirmation() {
		return confirmation;
	}

	public void setConfirmation(String confirmation) {
		this.confirmation = confirmation;
	}
}