package jp.alhinc.sato_keisuke.bulletin_board_system.model;

import java.io.Serializable;

public class userBean implements Serializable {
  private String loginId;
  private String password;
  private String name;
  private int store;
  private int position;
  private int user;
  private Boolean auth;

  /** Creates a new instance of UserBean */
    public userBean() {
    	loginId = "";
    	password = "";
    	name = "";
    	store = -1;
    	position = -1;
    	user = 0;
    	auth = false;
    }

    public String getLoginId() {
    	return loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Boolean getAuth() {
		return auth;
	}

	public void setAuth(Boolean auth) {
		this.auth = auth;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getStore() {
		return store;
	}

	public void setStore(int store) {
		this.store = store;
	}

	public int getPosition() {
		return position;
	}

	public void setPosition(int position) {
		this.position = position;
	}

	public int getUser() {
		return user;
	}

	public void setUser(int user) {
		this.user = user;
	}
}