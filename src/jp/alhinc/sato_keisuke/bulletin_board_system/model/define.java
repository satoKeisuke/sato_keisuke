package jp.alhinc.sato_keisuke.bulletin_board_system.model;

public class define {
	//ユーザー
	public final String loginId = "login_Id";
	public final String password = "password";
	public final String name = "name";
	public final String store = "store";
	public final String position = "departments_positions";
	public final String user = "user";

	//投稿
	public final String subject = "subject";
	public final String text = "text";
	public final String category = "category";
	public final String registered_date = "registered_date";
	public final String registered_user = "registered_user";
	public final String id = "id";

	//コメント
	public final String commentText = "commentText";


	public int getMinLength(String column) {
		int minLength = -1;

		if (loginId.equals(column)) {
			minLength = 6;
		} else if (password.equals(column)) {
			minLength = 6;
		} else if (name.equals(column)) {
			minLength = 0;
		} else if (subject.equals(column)) {
			minLength = 1;
		} else if (text.equals(column)) {
			minLength = 1;
		} else if (category.equals(column)) {
			minLength = 1;
		} else if (commentText.equals(column)) {
			minLength = 1;
		}

		return minLength;
	}

	public int getMaxLength(String column) {
		int maxLength = -1;

		if (loginId.equals(column)) {
			maxLength = 20;
		} else if (password.equals(column)) {
			maxLength = 20;
		} else if (name.equals(column)) {
			maxLength = 10;
		} else if (subject.equals(column)) {
			maxLength = 30;
		} else if (text.equals(column)) {
			maxLength = 1000;
		} else if (category.equals(column)) {
			maxLength = 10;
		} else if (commentText.equals(column)) {
			maxLength = 500;
		}

		return maxLength;
	}

	public String getPattern(String column) {
		String pattern = "";

		if (loginId.equals(column)) {
			pattern = "^[\\p{Alnum}]+$";
		} else if (password.equals(column)) {
			pattern = "^[\\p{Alnum}|\\p{Punct}]+$";
		} else if (name.equals(column)) {
			pattern = "";
		}
		return pattern;
	}

	public boolean checkFormat (String str, String column) {
		boolean result = false;
		if ((str.length() >= getMinLength(column) || getMinLength(column) == -1)
				&& (str.length() <= getMaxLength(column)) || getMaxLength(column) == -1) {

			if (getPattern(column).equals("") || str.matches(getPattern(column))) {
				result = true;
			}
		}
		return result;
	}
}
