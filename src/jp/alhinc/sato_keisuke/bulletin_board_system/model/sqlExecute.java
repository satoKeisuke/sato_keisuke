package jp.alhinc.sato_keisuke.bulletin_board_system.model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class sqlExecute {
	private static final String dbUrl = "jdbc:mysql://localhost:3306/sato_keisuke?characterEncoding=UTF-8&serverTimezone=JST";
	private static final String dbUser = "root";
	private static final String dbPassword = "Ksato0427";
	Connection connection = null;
	Statement statement = null;

	public ResultSet executeQuery(String sql) {
		ResultSet result;
		try{
			connection = DriverManager.getConnection(dbUrl, dbUser, dbPassword);
			statement = connection.createStatement();
			result = statement.executeQuery(sql);
		}catch (SQLException e){
			e.printStackTrace();
			System.out.println("接続失敗");
			result = null;
		}

		return result;
	}

	public int executeUpdate(String sql) {
		int result;

		try{
			connection = DriverManager.getConnection(dbUrl, dbUser, dbPassword);
			statement = connection.createStatement();
			result = statement.executeUpdate(sql);
		}catch (SQLException e){
			e.printStackTrace();
			System.out.println("接続失敗");
			result = -1;
		}

		return result;

	}

	public ArrayList<Integer> executeUpdateList(ArrayList<String> sqls) {
		ArrayList<Integer> resultList = new ArrayList<>();

		try{
			connection = DriverManager.getConnection(dbUrl, dbUser, dbPassword);
			statement = connection.createStatement();
			for (int i = 0; i < sqls.size(); i++) {
				int result;
				result = statement.executeUpdate(sqls.get(i));
				resultList.add(result);
			}
		}catch (SQLException e){
			e.printStackTrace();
			System.out.println("接続失敗");
			resultList.add(-1);
		}

		return resultList;

	}

	public void closeConcection () {
		try {
			if (connection.isClosed()) {

			} else {
				connection.close();
				statement.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
