package jp.alhinc.sato_keisuke.bulletin_board_system.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.alhinc.sato_keisuke.bulletin_board_system.model.userBean;

/**
 * Servlet implementation class homeContoroller
 */
@WebServlet("/completController")
public class completController extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public completController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		System.out.println( new Object(){}.getClass().getEnclosingClass().getName() + " " + new Object(){}.getClass().getEnclosingMethod().getName());

		String view = "";
		HttpSession session = request.getSession(true);
		if ((userBean)session.getAttribute("user") != null) {
			view = "/WEB-INF/view/completView.jsp";
		} else {
			view = "/WEB-INF/view/loginView.jsp";
		}
		RequestDispatcher dispatcher = request.getRequestDispatcher(view);
	    dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		System.out.println( new Object(){}.getClass().getEnclosingClass().getName() + " " + new Object(){}.getClass().getEnclosingMethod().getName());
		doGet(request, response);
	}

}
