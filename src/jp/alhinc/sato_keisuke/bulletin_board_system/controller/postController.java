package jp.alhinc.sato_keisuke.bulletin_board_system.controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.alhinc.sato_keisuke.bulletin_board_system.model.define;
import jp.alhinc.sato_keisuke.bulletin_board_system.model.error;
import jp.alhinc.sato_keisuke.bulletin_board_system.model.postBean;
import jp.alhinc.sato_keisuke.bulletin_board_system.model.sqlExecute;
import jp.alhinc.sato_keisuke.bulletin_board_system.model.sqls;
import jp.alhinc.sato_keisuke.bulletin_board_system.model.userBean;

/**
 * Servlet implementation class postController
 */
@WebServlet("/postController")
public class postController extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public postController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		System.out.println( new Object(){}.getClass().getEnclosingClass().getName() + " " + new Object(){}.getClass().getEnclosingMethod().getName());

		String view = "";
		HttpSession session = request.getSession(true);
		if ((userBean)session.getAttribute("user") != null) {
			view = "/WEB-INF/view/postView.jsp";
		} else {
			view = "/WEB-INF/view/loginView.jsp";
		}
	    RequestDispatcher dispatcher = request.getRequestDispatcher(view);

	    dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		System.out.println( new Object(){}.getClass().getEnclosingClass().getName() + " " + new Object(){}.getClass().getEnclosingMethod().getName());
		HttpSession session = request.getSession(true);
		request.setCharacterEncoding("UTF-8");

		error error = new error();
        errorController errorController = new errorController();

        postBean post = new postBean();

		try {
			Calendar cal = Calendar.getInstance();
	        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

			String subject = request.getParameter("subject");
			String text = request.getParameter("text");
			String category = request.getParameter("category");
			String regiDate = sdf.format(cal.getTime());
			String regiUser = ((userBean)session.getAttribute("user")).getLoginId();

	        post.setSubject(subject);
	        post.setText(text);
	        post.setCategory(category);
	        session.setAttribute("post", post);

	        define define = new define();
	        error.setSubject(define.checkFormat(subject, define.subject));
	        error.setText(define.checkFormat(text, define.text));
	        error.setCategory(define.checkFormat(category, define.category));
	        session.setAttribute("error", error);
	        if (error.getSubject() && error.getText() && error.getCategory()) {
		        sqls sqls = new sqls();
				String sql = sqls.getPostSql(subject, text, category, regiDate, regiUser);
				sqlExecute execute = new sqlExecute();
		        int result = 0;
				result = execute.executeUpdate(sql);

				if (result == 1) {
					session.setAttribute("post", null);
					completController complet = new completController();
					complet.doGet(request, response);
				} else {
					request.setAttribute("errorMessage", error.postErr);
					errorController.doGet(request, response);
				}
	        } else {
	        	doGet(request, response);
	        }

		} catch (Exception e) {
			request.setAttribute("errorMessage", error.postErr);
			errorController.doGet(request, response);
		}
	}

}
