package jp.alhinc.sato_keisuke.bulletin_board_system.controller;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.swing.JOptionPane;

import jp.alhinc.sato_keisuke.bulletin_board_system.model.define;
import jp.alhinc.sato_keisuke.bulletin_board_system.model.error;
import jp.alhinc.sato_keisuke.bulletin_board_system.model.sqlExecute;
import jp.alhinc.sato_keisuke.bulletin_board_system.model.sqls;
import jp.alhinc.sato_keisuke.bulletin_board_system.model.userBean;

/**
 * Servlet implementation class postController
 */
@WebServlet("/managementController")
public class managementController extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public managementController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		System.out.println( new Object(){}.getClass().getEnclosingClass().getName() + " " + new Object(){}.getClass().getEnclosingMethod().getName());

		HttpSession session = request.getSession(true);
		userBean user = new userBean();
		user = (userBean)session.getAttribute("user");

		String view = "";
		ArrayList<String[]> userList = new ArrayList<>();
		if ((userBean)session.getAttribute("user") != null) {
			if ((user.getPosition() == 1) && (user.getStore() == 1)) {
				view = "/WEB-INF/view/managementView.jsp";

				try {
					sqls sqls = new sqls();
					String sql = sqls.getUsersSql();
					sqlExecute execute = new sqlExecute();
		            ResultSet result = execute.executeQuery(sql);
		            define define = new define();
					while(result.next()){
						String[] userArray = new String[6];
						userArray[0] = result.getString(define.loginId);
						userArray[1] = result.getString(define.password);
						userArray[2] = result.getString(define.name);
						userArray[3] = result.getString(define.store);
						userArray[4] = result.getString(define.position);
						userArray[5] = result.getString(define.user);
						userList.add(userArray);
					}
					request.setAttribute("userList", userList);

				} catch (SQLException e) {
					e.printStackTrace();
				}

				RequestDispatcher dispatcher = request.getRequestDispatcher(view);
			    dispatcher.forward(request, response);
			} else {
				request.setAttribute("errorMessage", "権限がありません");
				errorController error = new errorController();
				error.doGet(request, response);
			}
		} else {
			view = "/WEB-INF/view/loginView.jsp";
			RequestDispatcher dispatcher = request.getRequestDispatcher(view);
		    dispatcher.forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		System.out.println( new Object(){}.getClass().getEnclosingClass().getName() + " " + new Object(){}.getClass().getEnclosingMethod().getName());

		System.out.println(request.getParameter("loginId"));
		System.out.println(request.getParameter("user"));
		String message = "";
		if (request.getParameter("user").equals("0")) {
			message = request.getParameter("loginId") + "を復活させますか？";
		} else {
			message = request.getParameter("loginId") + "を停止させますか？";
		}

		int confResult = 0;
		int sqlResult = 0;
		confResult = JOptionPane.showConfirmDialog(null, message, "確認", 0);
		if (confResult == 0) {
			sqls sqls = new sqls();
			String sql = sqls.getChangeSql(request.getParameter("loginId"), request.getParameter("user"));
			sqlExecute execute = new sqlExecute();
	        sqlResult = execute.executeUpdate(sql);
		}
		if (sqlResult == -1) {
        	error error = new error();
	        errorController errorController = new errorController();
			request.setAttribute("errorMessage", error.updateErr);
			errorController.doGet(request, response);
        } else {
        	doGet(request, response);
        }
	}

}
