package jp.alhinc.sato_keisuke.bulletin_board_system.controller;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.alhinc.sato_keisuke.bulletin_board_system.model.sqlExecute;
import jp.alhinc.sato_keisuke.bulletin_board_system.model.sqls;
import jp.alhinc.sato_keisuke.bulletin_board_system.model.storePositionBean;
import jp.alhinc.sato_keisuke.bulletin_board_system.model.userBean;

/**
 * Servlet implementation class loginController
 */
@WebServlet("/loginController")
public class loginController extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public loginController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		System.out.println( new Object(){}.getClass().getEnclosingClass().getName() + " " + new Object(){}.getClass().getEnclosingMethod().getName());

		String view = "";
		userBean user = new userBean();
		HttpSession session = request.getSession(true);
		user = (userBean)session.getAttribute("user");

		if (((userBean)session.getAttribute("user") != null) && (user.getAuth())) {
			homeController home = new homeController();
			home.doGet(request, response);
		} else {
			view = "/WEB-INF/view/loginView.jsp";
			RequestDispatcher dispatcher = request.getRequestDispatcher(view);
		    dispatcher.forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		System.out.println( new Object(){}.getClass().getEnclosingClass().getName() + " " + new Object(){}.getClass().getEnclosingMethod().getName());

		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");

		userBean user = new userBean();
		HttpSession session = request.getSession(true);
		storePositionBean storePosition = new storePositionBean();
		try{
			sqls sqls = new sqls();

			String sql = sqls.getLoginSql(loginId, password);

			sqlExecute execute = new sqlExecute();
            ResultSet result = execute.executeQuery(sql);

            while(result.next()){
                if (result.getInt("user") == 1) {
	                user.setAuth(true);
	                user.setLoginId(result.getString("login_id"));
	    			user.setPassword(result.getString("password"));
	    			user.setName(result.getString("name"));
	    			user.setStore(result.getInt("store"));
	    			user.setPosition(result.getInt("departments_positions"));
                }
            }
            execute.closeConcection();

		}catch (SQLException e){
			e.printStackTrace();
			System.out.println("接続失敗");
		}

		if (user.getAuth()) {
			session.setAttribute("user", user);
			session.setAttribute("storePosition", storePosition);
			try {
				homeController home = new homeController();
				home.doGet(request, response);
			} catch (Exception e) {
				errorController error = new errorController();
				request.setAttribute("errorMessage", "予期せぬエラー");
				error.doGet(request, response);
			}
		} else {
			request.setAttribute("auth", "0");
			doGet(request,response);
		}
	}
}
