package jp.alhinc.sato_keisuke.bulletin_board_system.controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.alhinc.sato_keisuke.bulletin_board_system.model.define;
import jp.alhinc.sato_keisuke.bulletin_board_system.model.error;
import jp.alhinc.sato_keisuke.bulletin_board_system.model.sqlExecute;
import jp.alhinc.sato_keisuke.bulletin_board_system.model.sqls;
import jp.alhinc.sato_keisuke.bulletin_board_system.model.userBean;
import jp.alhinc.sato_keisuke.bulletin_board_system.model.userFieldBean;

/**
 * Servlet implementation class postController
 */
@WebServlet("/editController")
public class editController extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public editController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		System.out.println( new Object(){}.getClass().getEnclosingClass().getName() + " " + new Object(){}.getClass().getEnclosingMethod().getName());

		HttpSession session = request.getSession(true);
		userBean user = new userBean();
		user = (userBean)session.getAttribute("user");
		String view;

		if ((userBean)session.getAttribute("user") != null) {
			if ((user.getPosition() == 1) && (user.getStore() == 1)) {
				view = "/WEB-INF/view/editView.jsp";
				RequestDispatcher dispatcher = request.getRequestDispatcher(view);
			    dispatcher.forward(request, response);
			} else {
				request.setAttribute("errorMessage", "権限がありません");
				errorController error = new errorController();
				error.doGet(request, response);
			}
		} else {
			view = "/WEB-INF/view/loginView.jsp";
			RequestDispatcher dispatcher = request.getRequestDispatcher(view);
		    dispatcher.forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		System.out.println( new Object(){}.getClass().getEnclosingClass().getName() + " " + new Object(){}.getClass().getEnclosingMethod().getName());
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession(true);
		userBean oldUser = new userBean();
		userFieldBean newUser = new userFieldBean();
		error error = new error();

		String control = "";
		if (request.getParameter("control") != null) {
			control = request.getParameter("control");
		}

		if (session.getAttribute("oldUser") == null || control.equals("management")) {
			//旧情報
			oldUser.setLoginId(request.getParameter("oldLoginId"));
			oldUser.setPassword(request.getParameter("oldPassword"));
			oldUser.setName(request.getParameter("oldName"));
			oldUser.setStore(Integer.parseInt(request.getParameter("oldStore")));
			oldUser.setPosition(Integer.parseInt(request.getParameter("oldPosition")));
			session.setAttribute("oldUser", oldUser);
			session.setAttribute("newUser", newUser);
			session.setAttribute("error", error);
			doGet(request, response);
		} else {
			//新情報
			String newLoginId = request.getParameter("newLoginId");
			newUser.setLoginId(newLoginId);
			String newPassword = request.getParameter("newPassword");
			newUser.setPassword(newPassword);
			String newName = request.getParameter("newName");
			newUser.setName(newName);
			String newStore = request.getParameter("newStore");
			newUser.setStore(newStore);
			String newPosition = request.getParameter("newPosition");
			newUser.setPosition(newPosition);

			//定義チェック
			if (newStore != null && !newStore.equals("")) {
				try {
					Integer.parseInt(newStore);
				} catch (NumberFormatException e) {
					error.setStore(false);
					System.out.println("store_error");
				}
			}
			if (newPosition != null && !newPosition.equals("")) {
				try {
					Integer.parseInt(newPosition);
				} catch (NumberFormatException e) {
					error.setPosition(false);
					System.out.println("position_error");
				}
			}
			if (!newUser.getPassword().equals(request.getParameter("newConfirmation"))) {
				error.setConfirmation(false);
			}
			define define = new define();
			if (!define.checkFormat(newUser.getPassword(), define.password) && !newUser.getPassword().equals("")) {
				error.setPassword(false);
			}
			if (!define.checkFormat(newUser.getLoginId(), define.loginId) && !newUser.getLoginId().equals("")) {
				error.setLoginId(false);
			}
			if (!define.checkFormat(newUser.getName(), define.name) && !newUser.getName().equals("")) {
				error.setName(false);
			}
			session.setAttribute("newUser", newUser);
			session.setAttribute("error", error);

			if (error.getLoginId() && error.getName() && error.getPassword() && error.getStore() && error.getPosition() && error.getConfirmation()) {
				sqls sqls = new sqls();
				sqlExecute execute = new sqlExecute();
				ArrayList<String> sqlList = new ArrayList<>();
				String oldLoginId = ((userBean)session.getAttribute("oldUser")).getLoginId();
				sqlList = sqls.getEditSql(oldLoginId, newUser.getLoginId(), newUser.getPassword(), newUser.getName(), newUser.getStore(), newUser.getPosition());
				ArrayList<Integer> result = new ArrayList<>();
				result = execute.executeUpdateList(sqlList);

				if (result.contains(0) || result.contains(-1)) {
					errorController errorController = new errorController();
					request.setAttribute("errorMessage", error.updateErr);
					errorController.doGet(request, response);
				} else {
					completController complet = new completController();
					complet.doGet(request, response);
				}
			} else {
				doGet(request, response);
			}
		}
	}
}
