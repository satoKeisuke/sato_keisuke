package jp.alhinc.sato_keisuke.bulletin_board_system.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.alhinc.sato_keisuke.bulletin_board_system.model.define;
import jp.alhinc.sato_keisuke.bulletin_board_system.model.sqlExecute;
import jp.alhinc.sato_keisuke.bulletin_board_system.model.sqls;
import jp.alhinc.sato_keisuke.bulletin_board_system.model.userBean;

/**
 * Servlet implementation class homeContoroller
 */
@WebServlet("/signUpController")
public class signUpController extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public signUpController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		System.out.println( new Object(){}.getClass().getEnclosingClass().getName() + " " + new Object(){}.getClass().getEnclosingMethod().getName());
		HttpSession session = request.getSession(true);
		userBean user = new userBean();
		user = (userBean)session.getAttribute("user");
		String view;
		if ((userBean)session.getAttribute("user") != null) {
			if ((user.getPosition() == 1) && (user.getStore() == 1)) {
				view = "/WEB-INF/view/signUpView.jsp";
				RequestDispatcher dispatcher = request.getRequestDispatcher(view);
			    dispatcher.forward(request, response);
			} else {
				request.setAttribute("errorMessage", "権限がありません");
				errorController error = new errorController();
				error.doGet(request, response);
			}
		} else {
			view = "/WEB-INF/view/loginView.jsp";
			RequestDispatcher dispatcher = request.getRequestDispatcher(view);
		    dispatcher.forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		System.out.println( new Object(){}.getClass().getEnclosingClass().getName() + " " + new Object(){}.getClass().getEnclosingMethod().getName());
		String loginId;
		String password;
		String confirmation;
		String name;
		int store;
		int position;

		boolean inputErr = true;
		request.setCharacterEncoding("UTF-8");

		define define = new define();

		try {
			password = request.getParameter("password");
			confirmation = request.getParameter("confirmation");

			if (password.equals(confirmation)) {

				loginId = request.getParameter("loginId");
				name = request.getParameter("name");

				if ((define.checkFormat(password, define.password))
						&& (define.checkFormat(loginId, define.loginId))
						&& (define.checkFormat(name, define.name))) {
				} else {
					throw new Exception();
				}

				//数値のみ
				store = Integer.parseInt(request.getParameter("store"));
				//数値のみ
				position = Integer.parseInt(request.getParameter("position"));

				sqls sqls = new sqls();
				String sql = sqls.getSignUpSql(loginId, password, name, store, position);
				sqlExecute execute = new sqlExecute();
		        int result = execute.executeUpdate(sql);

		        if ((result == 1) && (inputErr)) {
		        	System.out.println("登録成功");
		        	managementController management = new managementController();
		        	management.doGet(request, response);

		        } else {
		        	inputErr = false;
		        }
			}
		} catch(NumberFormatException e) {
			inputErr = false;
			e.printStackTrace();
		} catch(Exception e) {
			inputErr = false;
			e.printStackTrace();
		}

		if (inputErr == false) {
			errorController error = new errorController();
			request.setAttribute("errorMessage", "入力値が異常です");
			error.doGet(request, response);
		}
	}
}
