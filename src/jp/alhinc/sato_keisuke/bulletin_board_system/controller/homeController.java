package jp.alhinc.sato_keisuke.bulletin_board_system.controller;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.swing.JOptionPane;

import jp.alhinc.sato_keisuke.bulletin_board_system.model.define;
import jp.alhinc.sato_keisuke.bulletin_board_system.model.homeBean;
import jp.alhinc.sato_keisuke.bulletin_board_system.model.sqlExecute;
import jp.alhinc.sato_keisuke.bulletin_board_system.model.sqls;
import jp.alhinc.sato_keisuke.bulletin_board_system.model.userBean;

/**
 * Servlet implementation class homeContoroller
 */
@WebServlet("/homeController")
public class homeController extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public homeController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		System.out.println( new Object(){}.getClass().getEnclosingClass().getName() + " " + new Object(){}.getClass().getEnclosingMethod().getName());
		HttpSession session = request.getSession(true);

		String view = "";
		if ((userBean)session.getAttribute("user") != null) {
			ArrayList<String[][]> postList = new ArrayList<>();
			ArrayList<ArrayList<String[]>[]> commentList = new ArrayList<>();

			view = "/WEB-INF/view/homeView.jsp";
			homeBean home = new homeBean();
			try {
				sqls sqls = new sqls();
				String sql = "";
				if (session.getAttribute("home") != null) {
					home = (homeBean)session.getAttribute("home");
				}

				sql = sqls.getPostsFilterSql(home.getCategory(), home.getFromTerm(), home.getToTerm());

				if (request.getParameter("jumpPageNum") != null) {
					home.setCurrentPage(Integer.parseInt(request.getParameter("jumpPageNum")));
					session.setAttribute("home", home);
				} else {
					home.setCurrentPage(1);
					session.setAttribute("home", home);
				}

				sqlExecute postExecute = new sqlExecute();
				sqlExecute commentExecute = new sqlExecute();
	            ResultSet postResult = postExecute.executeQuery(sql);
	            define define = new define();
	            int count = 0;
	            String[][] postArray = new String[5][6];
	            ArrayList<String[]>[] commentArray = new ArrayList[5];
				while (postResult.next()) {
					postArray[count][0] = postResult.getString(define.subject);
					postArray[count][1] = postResult.getString(define.text);
					postArray[count][2] = postResult.getString(define.category);
					postArray[count][3] = postResult.getString(define.registered_date);
					postArray[count][4] = postResult.getString(define.registered_user);
					postArray[count][5] = postResult.getString(define.id);

					sql = sqls.getCommentsSql(postResult.getString(define.id));
					ResultSet commentResult = commentExecute.executeQuery(sql);

					ArrayList<String[]> commentRecordArray = new ArrayList<>();
					while (commentResult.next()) {
						String[] commentRecord = new String[4];
						commentRecord[0] = commentResult.getString(define.text);
						commentRecord[1] = commentResult.getString(define.registered_date);
						commentRecord[2] = commentResult.getString(define.registered_user);
						commentRecord[3] = commentResult.getString(define.id);


						commentRecordArray.add(commentRecord.clone());
					}
					commentArray[count] = commentRecordArray;
					commentExecute.closeConcection();

					if (count == 4) {
						//１ページに５投稿表示のためリセット
						postList.add(postArray.clone());
						postArray = new String[5][6];
						commentList.add(commentArray.clone());
						commentArray = new ArrayList[5];
						count = 0;
					} else {
						count++;
					}
				}
				postExecute.closeConcection();

				if (0 < count && count < 4) {
					postList.add(postArray.clone());
					commentList.add(commentArray.clone());
				}
				request.setAttribute("comment", request.getParameter("comment"));
				request.setAttribute("postList", postList);
				request.setAttribute("commentList", commentList);

			} catch (SQLException e) {
				e.printStackTrace();
			}
		} else {
			view = "/WEB-INF/view/loginView.jsp";
		}
		RequestDispatcher dispatcher = request.getRequestDispatcher(view);
	    dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		System.out.println( new Object(){}.getClass().getEnclosingClass().getName() + " " + new Object(){}.getClass().getEnclosingMethod().getName());
		request.setCharacterEncoding("UTF-8");
		String requestAct = request.getParameter("pagename");
		HttpSession session = request.getSession(true);
		homeBean home = new homeBean();

		boolean skipFlg = false;
		if (requestAct.equals("検索")) {
			//検索した場合
			home.setCategory(request.getParameter("category"));
			home.setFromTerm(request.getParameter("fromYear") + "-" + request.getParameter("fromMonth") + "-" + request.getParameter("fromDay") + " 00:00:00");
			home.setToTerm(request.getParameter("toYear") + "-" + request.getParameter("toMonth") + "-" + request.getParameter("toDay") + " 23:59:59");
			session.setAttribute("home", home);
		} else if (requestAct.equals("コメント")) {
			//コメントした場合
			sqlExecute execute = new sqlExecute();
			sqls sqls = new sqls();

			Calendar cal = Calendar.getInstance();
	        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String comment = request.getParameter("comment");

			define define = new define();
			if (define.checkFormat (comment, define.commentText)) {
				String regiDate = sdf.format(cal.getTime());
				String regiUser = ((userBean)session.getAttribute("user")).getLoginId();
				String postId = request.getParameter("postId");

				String sql = sqls.getPostCommentSql(comment, regiDate, regiUser, postId);
	            int result = execute.executeUpdate(sql);
	            execute.closeConcection();
			} else {
				JOptionPane.showMessageDialog(null, "文字数エラーです", "違反", 0);
			}
		} else if (requestAct.equals("削除")) {
			//削除した場合
			String target = request.getParameter("target");
			sqlExecute execute = new sqlExecute();
			sqls sqls = new sqls();
			String sql = "";
			int result = 0;
			if (target.equals("post")) {
				sql = sqls.getPostDeleteSql(request.getParameter("id"));
				result = execute.executeUpdate(sql);
				//紐づくコメントも削除
				sql = sqls.getPostCommentDeleteSql(request.getParameter("id"));
				result = execute.executeUpdate(sql);

			} else if (target.equals("comment")) {
				sql = sqls.getCommentDeleteSql(request.getParameter("id"));
				result = execute.executeUpdate(sql);
			}
            execute.closeConcection();
		}
		doGet(request, response);
	}

}
