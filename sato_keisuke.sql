-- MySQL dump 10.13  Distrib 8.0.13, for Win64 (x86_64)
--
-- Host: localhost    Database: sato_keisuke
-- ------------------------------------------------------
-- Server version	8.0.13

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8mb4 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `comments`
--

DROP TABLE IF EXISTS `comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `comments` (
  `text` varchar(500) NOT NULL,
  `registered_date` datetime DEFAULT NULL,
  `registered_user` varchar(10) DEFAULT NULL,
  `post_id` int(11) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comments`
--

LOCK TABLES `comments` WRITE;
/*!40000 ALTER TABLE `comments` DISABLE KEYS */;
INSERT INTO `comments` VALUES ('あ','2018-12-22 21:02:31','admin',8,2),('あああ','2018-12-22 21:19:04','admin',26,5),('adminda','2018-12-23 14:47:02','admin',1,6),('yo','2018-12-23 14:47:09','admin',1,7),('あ','2018-12-23 15:31:28','admin',22,11),('コメント\r\nコメント','2018-12-23 15:41:49','admin',23,12),('コメント','2018-12-23 15:44:16','admin',18,13),('a','2018-12-23 18:33:39','tester2',31,14),('a','2018-12-23 18:36:23','tester2',18,15),('a','2018-12-23 18:36:30','tester2',18,16),('a','2018-12-24 09:20:12','admin',36,18);
/*!40000 ALTER TABLE `comments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `departments_positions`
--

DROP TABLE IF EXISTS `departments_positions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `departments_positions` (
  `name` varchar(255) NOT NULL,
  `code` int(11) NOT NULL,
  PRIMARY KEY (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `departments_positions`
--

LOCK TABLES `departments_positions` WRITE;
/*!40000 ALTER TABLE `departments_positions` DISABLE KEYS */;
INSERT INTO `departments_positions` VALUES ('社員',0),('総務人事担当者',1),('情報管理担当者',2),('支店長',3);
/*!40000 ALTER TABLE `departments_positions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `posts`
--

DROP TABLE IF EXISTS `posts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `posts` (
  `subject` varchar(30) NOT NULL,
  `text` varchar(1000) NOT NULL,
  `category` varchar(10) NOT NULL,
  `registered_date` datetime DEFAULT NULL,
  `registered_user` varchar(10) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `posts`
--

LOCK TABLES `posts` WRITE;
/*!40000 ALTER TABLE `posts` DISABLE KEYS */;
INSERT INTO `posts` VALUES ('テスト','テスト','テスト','2018-12-22 21:00:17','admin',1),('あ','あ','サンプル','2018-12-21 13:47:31','admin',2),('あ','あ','サンプル','2018-12-21 13:47:31','admin',3),('あ','あ','サンプル','2018-12-21 13:47:31','admin',4),('あ','あ','サンプル','2018-12-21 13:47:31','admin',5),('あ','あ','サンプル','2018-12-21 13:47:31','admin',6),('あ','あ','サンプル','2018-12-20 13:47:31','tester',7),('あ','あ','サンプル','2018-12-20 13:47:31','tester',8),('あ','あ','サンプル','2018-12-20 13:47:31','tester',9),('あ','あ','サンプル','2018-12-20 13:47:31','tester',10),('あ','あ','サンプル','2018-12-22 13:47:31','tester1',11),('あ','あ','サンプル','2018-12-22 13:47:31','tester1',12),('あ','あ','サンプル','2018-12-22 13:47:31','tester1',13),('あ','あ','サンプル','2018-12-22 13:47:31','tester2',14),('あ','あ','サンプル','2018-12-22 13:47:31','tester2',15),('あ','あ','サンプル','2018-12-22 13:47:31','tester2',16),('あ','あ','サンプル','2018-12-22 13:47:31','tester2',17),('あ','あ','サンプル','2018-12-22 13:47:31','admin',18),('あ','あ','サンプル','2018-12-22 13:47:31','admin',19),('あ','あ','サンプル','2018-12-22 13:47:31','admin',20),('あ','あ','サンプル','2018-12-22 13:47:31','admin',21),('あ','あ','サンプル','2018-12-22 13:47:31','admin',22),('あ','あ','サンプル','2018-12-22 13:47:31','admin',23),('あ','あ','サンプル','2018-12-22 13:47:31','admin',24),('あ','あ','サンプル','2018-12-22 13:47:31','admin',25),('あ','あ','サンプル','2018-12-22 13:47:31','admin',26),('あ','あ','サンプル','2018-12-22 13:47:31','admin',27),('あ','あ','サンプル','2018-12-22 13:47:31','admin',28),('投稿','重要だよ','重要','2018-12-23 15:06:08','admin',29),('重要','重要だよ\r\n重要だよ','重要','2018-12-23 15:06:29','admin',30),('あ','ああ\r\nああ\r\nああ','あ','2018-12-23 15:07:54','admin',31),('あ','あああ\r\nあああ\r\nあああ\r\nあああ','あ','2018-12-23 15:10:14','admin',32),('あ','ああ\r\nああ\r\nああ','あ','2018-12-23 15:13:32','admin',33),('a','a','a','2018-12-23 18:37:00','tester2',36);
/*!40000 ALTER TABLE `posts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stores`
--

DROP TABLE IF EXISTS `stores`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `stores` (
  `name` varchar(255) NOT NULL,
  `code` int(11) NOT NULL,
  PRIMARY KEY (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stores`
--

LOCK TABLES `stores` WRITE;
/*!40000 ALTER TABLE `stores` DISABLE KEYS */;
INSERT INTO `stores` VALUES ('-',0),('本社',1),('支店A',2),('支店B',3),('支店C',4);
/*!40000 ALTER TABLE `stores` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `users` (
  `login_id` varchar(20) NOT NULL,
  `password` varchar(20) NOT NULL,
  `name` varchar(10) DEFAULT NULL,
  `store` int(11) DEFAULT NULL,
  `departments_positions` int(11) DEFAULT NULL,
  `user` int(11) DEFAULT NULL,
  PRIMARY KEY (`login_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES ('admin','admin','管理者',1,1,1),('guest','guest','ゲスト',2,0,1),('tester1','tester1','テスター１',2,3,0),('tester2','tester2','テスター２',1,1,1),('tester3','tester3','テスター３',4,3,1),('tester4','tester4','テスター４',2,1,1);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-12-24 12:39:08
